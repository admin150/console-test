<?php

namespace Lib\Test\Domain;

use PHPUnit\Framework\TestCase;

class CoolClassTest extends TestCase
{
    private CoolClass $cool_class;

    public function setUp(): void
    {
        parent::setUp();
        $this->cool_class = new CoolClass();
    }

    public function testBeCool()
    {
        $this->assertEquals('ok', $this->cool_class->beCool());
    }
}
