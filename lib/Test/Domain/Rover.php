<?php


namespace Lib\Test\Domain;


class Rover
{
    private int $position_x;
    private int $position_y;
    private string $heading;

    public function __construct(int $position_x = 0, int $position_y = 0, string $heading = 'N')
    {
        $this->position_x = $position_x;
        $this->position_y = $position_y;
        $this->heading = $heading;
    }

    public function getCoords(): array
    {
        return [$this->position_x, $this->position_y];
    }

    public function getHeading(): string
    {
        return $this->heading;
    }

    public function moveForward(): array
    {
        switch ($this->heading) {
            case 'N':
                $this->position_y += 1;
                break;
            case 'E':
                $this->position_x += 1;
                break;
            case 'S':
                $this->position_y -= 1;
                break;
            case 'W':
                $this->position_x -= 1;
                break;
        }

        return $this->getCoords();
    }

    public function moveBackward(): array
    {
        switch ($this->heading) {
            case 'N':
                $this->position_y -= 1;
                break;
            case 'E':
                $this->position_x -= 1;
                break;
            case 'S':
                $this->position_y += 1;
                break;
            case 'W':
                $this->position_x += 1;
                break;
        }

        return $this->getCoords();
    }

    public function turnRight(): string
    {
        switch ($this->heading) {
            case 'N':
                $this->heading = 'E';
                break;
            case 'E':
                $this->heading = 'S';
                break;
            case 'S':
                $this->heading = 'W';
                break;
            case 'W':
                $this->heading = 'N';
                break;
        }

        return $this->getHeading();
    }

    public function turnLeft(): string
    {
        switch ($this->heading) {
            case 'N':
                $this->heading = 'W';
                break;
            case 'W':
                $this->heading = 'S';
                break;
            case 'S':
                $this->heading = 'E';
                break;
            case 'E':
                $this->heading = 'N';
                break;
        }

        return $this->getHeading();
    }
}