<?php

namespace Lib\Test\Domain;

use PHPUnit\Framework\TestCase;

class RoverTest extends TestCase
{
    public function testStartingPosition()
    {
        $rover = new Rover();
        $starting_coords = $rover->getCoords();
        $starting_heading = $rover->getHeading();

        $this->assertEquals([0,0], $starting_coords);
        $this->assertEquals('N', $starting_heading);
    }

    public function testEastStartingPosition()
    {
        $rover = new Rover(0, 0, 'E');
        $starting_coords = $rover->getCoords();
        $starting_heading = $rover->getHeading();

        $this->assertEquals([0,0], $starting_coords);
        $this->assertEquals('E', $starting_heading);
    }

    public function testCustomStartingCoordinates()
    {
        $rover = new Rover(1, 1);
        $starting_coords = $rover->getCoords();
        $starting_heading = $rover->getHeading();

        $this->assertEquals([1, 1], $starting_coords);
        $this->assertEquals('N', $starting_heading);
    }

    public function testGetCoords()
    {
        $rover = new Rover();
        $expected = [0,0];
        $this->assertEquals($expected, $rover->getCoords());
    }

    public function testMoveForward()
    {
        $rover = new Rover();
        $new_position = $rover->moveForward();
        $expected = [0, 1];
        $this->assertEquals($expected, $new_position);
    }

    public function testMoveForwardTwice()
    {
        $rover = new Rover();
        $first_position = $rover->moveForward();
        $second_position = $rover->moveForward();
        $this->assertEquals([0, 1], $first_position);
        $this->assertEquals([0, 2], $second_position);
    }

    public function testMoveBackward()
    {
        $rover = new Rover();
        $new_position = $rover->moveBackward();
        $expected = [0, -1];
        $this->assertEquals($expected, $new_position);
    }

    public function testMoveBackwardTwice()
    {
        $rover = new Rover();
        $first_position = $rover->moveBackward();
        $second_position = $rover->moveBackward();
        $this->assertEquals([0, -1], $first_position);
        $this->assertEquals([0, -2], $second_position);
    }

    public function testGetHeading()
    {
        $rover = new Rover();
        $heading = $rover->getHeading();
        $this->assertEquals('N', $heading);
    }

    public function testTurnRight()
    {
        $rover = new Rover();
        $new_heading = $rover->turnRight();
        $this->assertEquals('E', $new_heading);
    }

    public function testTurnRightTwice()
    {
        $rover = new Rover();
        $first_heading = $rover->turnRight();
        $second_heading = $rover->turnRight();
        $this->assertEquals('E', $first_heading);
        $this->assertEquals('S', $second_heading);
    }

    public function testTurnRightThreeTimes()
    {
        $rover = new Rover();
        $first_heading = $rover->turnRight();
        $second_heading = $rover->turnRight();
        $third_heading = $rover->turnRight();
        $this->assertEquals('E', $first_heading);
        $this->assertEquals('S', $second_heading);
        $this->assertEquals('W', $third_heading);
    }

    public function testTurnRightFourTimes()
    {
        $rover = new Rover();
        $first_heading = $rover->turnRight();
        $second_heading = $rover->turnRight();
        $third_heading = $rover->turnRight();
        $fourth_heading = $rover->turnRight();
        $this->assertEquals('E', $first_heading);
        $this->assertEquals('S', $second_heading);
        $this->assertEquals('W', $third_heading);
        $this->assertEquals('N', $fourth_heading);
    }

    public function testTurnLeft()
    {
        $rover = new Rover();
        $new_heading = $rover->turnLeft();
        $this->assertEquals('W', $new_heading);
    }

    public function testTurnLeftTwice()
    {
        $rover = new Rover();
        $first_heading = $rover->turnLeft();
        $second_heading = $rover->turnLeft();
        $this->assertEquals('W', $first_heading);
        $this->assertEquals('S', $second_heading);
    }

    public function testTurnLeftThreeTimes()
    {
        $rover = new Rover();
        $first_heading = $rover->turnLeft();
        $second_heading = $rover->turnLeft();
        $third_heading = $rover->turnLeft();
        $this->assertEquals('W', $first_heading);
        $this->assertEquals('S', $second_heading);
        $this->assertEquals('E', $third_heading);
    }

    public function testTurnLeftFourTimes()
    {
        $rover = new Rover();
        $first_heading = $rover->turnLeft();
        $second_heading = $rover->turnLeft();
        $third_heading = $rover->turnLeft();
        $fourth_heading = $rover->turnLeft();
        $this->assertEquals('W', $first_heading);
        $this->assertEquals('S', $second_heading);
        $this->assertEquals('E', $third_heading);
        $this->assertEquals('N', $fourth_heading);
    }

    public function testMoveForwardAtHeading()
    {
        $rover = new Rover(0, 0, 'E');
        $new_coords = $rover->moveForward();

        $this->assertEquals([1, 0], $new_coords);
    }

    public function testMoveBackwardAtHeading()
    {
        $rover = new Rover(0, 0, 'E');
        $new_coords = $rover->moveBackward();

        $this->assertEquals([-1, 0], $new_coords);
    }

    public function testTurningAndMovingOnce()
    {
        $rover = new Rover();
        $rover->turnLeft();
        $rover->moveBackward();

        $coords = $rover->getCoords();
        $heading = $rover->getHeading();

        $this->assertEquals([1, 0], $coords);
        $this->assertEquals('W', $heading);
    }

    public function testTurningAndMoving()
    {
        $rover = new Rover();
        $rover->turnLeft();
        $rover->moveForward();
        $rover->moveForward();
        $rover->moveForward();
        $rover->turnLeft();
        $rover->moveBackward();
        $rover->moveBackward();
        $rover->moveBackward();

        $coords = $rover->getCoords();
        $heading = $rover->getHeading();

        $this->assertEquals([-3, 3], $coords);
        $this->assertEquals('S', $heading);
    }
}
