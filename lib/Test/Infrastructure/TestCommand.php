<?php


namespace Lib\Test\Infrastructure;

use Lib\Test\Domain\CoolClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    private CoolClass $cool;

    public function __construct(CoolClass $cool)
    {
        parent::__construct();

        $this->cool = $cool;
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'test';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = "Hello" . $this->cool->beCool();
        $output->write($message);

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;
    }
}