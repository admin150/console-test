<?php

require __DIR__ . '/vendor/autoload.php';

use Lib\Test\Infrastructure\TestCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

$container_builder = new ContainerBuilder();
$loader = new PhpFileLoader($container_builder, new FileLocator(__DIR__));
$loader->load('services.php');

$application = new Application();

// ... register commands
$application->add($container_builder->get(TestCommand::class));

$application->run();