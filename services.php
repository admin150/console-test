<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Lib\Test\Domain\CoolClass;
use Lib\Test\Infrastructure\TestCommand;

return function(ContainerConfigurator $configurator) {
    $services = $configurator->services();
    $services->set(TestCommand::class, TestCommand::class)
        ->args([service(CoolClass::class)]);

    $services->set(CoolClass::class, CoolClass::class);
};
